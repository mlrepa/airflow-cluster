# Prepare

## Create *.env*:
```dotenv
# Git
GIT_CONFIG_USER_NAME=<git_user>
GIT_CONFIG_EMAIL=<git_email>

# Airflow
AIRFLOW_BASE_RUN_DIR=/tmp/airflow

# MLflow
MLFLOW_STORAGE=/tmp/mlflow-storage
MLFLOW_TRACKING_URI=http://mlflow-server:1234

# Virtual environments
VENVS_FOLDER=/tmp/virtualenvs
```

**Notes**:
- папка `VENVS_FOLDER` содержит папки виртуальных окружений и должна быть создана перед запуском Airflow:
```bash
mkdir -p /tmp/virtualenvs
```

## Build Airflow image

```bash
chmod +x build.sh
./build.sh
```

## Run Airflow cluster

```bash
docker-compose up -d
```

## Stop Airflow cluster

```bash
docker-compose down
```

## Run DAGs

go to [airflow UI](http://localhost:8080).

*login*: airflow
*password*: airflow


# Monitoring

Enter Grafana UI: ```http://localhost:3000```

Login: `admin`

Password: `password`


## Add Prometheus data source

1. go to `Configuration` -> `Datasources`
2. click `Add data source`
3. select `Prometheus`
4. add `URL` = `http://prometheus:9090`
5. click `Save and test`

Test it:
- go to `Explore`
- select `Prometheus` and then - airflow metrics


## Add Airflow database (Postgres) data source

1. go to `Configuration` -> `Datasources`
2. click `Add data source`
3. select `PostgreSQL`
4. fill parameters:
   - `Name`: `airflow database`
   - `Host`: `airflow-db:5432`
   - `Database`: `airflow`
   - `User`: `airflow`
   - `Password`: `airflow`
   - `SSL Mode`: `disable`
   - `Version`: 12
5. click `Save and test`

Test it:
- go to `Explore`.
- select `airflow database`

## Add MLflow database (Postgres) data source

1. go to `Configuration` -> `Datasources`
2. click `Add data source`
3. select `PostgreSQL`
4. fill parameters:
   - `Name`: `mlflow database`
   - `Host`: `mlflow-db:5432`
   - `Database`: `mlflow`
   - `User`: `mlflow`
   - `Password`: `mlflow`
   - `SSL Mode`: `disable`
   - `Version`: 12
5. click `Save and test`

Test it:
- go to `Explore`
- select `mlflow database`


## Import tutorial dashboards

1. go to `+` -> `Import`
2. click `Upload JSON file`
3. select json dashboard file `./grafana/dashboards/tutorial-predict-device-change/tutorial-predict-device-change.json`
4. specify `airflow database`, `Prometheus` and `mlflow database`
5. click import

The same for dashboard `grafana/dashboards/tutorial-predict-device-change/model-performance-v1.json`