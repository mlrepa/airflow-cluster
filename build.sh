#!/usr/bin/env bash
export $(grep -v '#.*' .env | xargs)

export AIRFLOW_UID=$(id -u)
export AIRFLOW_GID=1000

docker-compose build
