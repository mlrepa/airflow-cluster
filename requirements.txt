apache-airflow[postgres,crypto,statsd,celery,redis]==2.0.1
gitpython==3.1.8
psycopg2-binary==2.8.6
pyarrow==3.0.0
SQLAlchemy==1.3.23