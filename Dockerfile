ARG PYTHON_BASE_IMAGE="python:3.8-slim"
ARG PYTHON_MAJOR_MINOR_VERSION="3.8"

##############################################################################################
# This is the build image where we build all dependencies
##############################################################################################
FROM ${PYTHON_BASE_IMAGE} as airflow-build-image
SHELL ["/bin/bash", "-o", "pipefail", "-e", "-u", "-x", "-c"]

# Make sure noninteractive debian install is used and language variables set
ENV DEBIAN_FRONTEND=noninteractive LANGUAGE=C.UTF-8 LANG=C.UTF-8 LC_ALL=C.UTF-8 \
    LC_CTYPE=C.UTF-8 LC_MESSAGES=C.UTF-8

# Install curl and gnupg2 - needed for many other installation steps
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
           apt-transport-https \
           apt-utils \
           build-essential \
           ca-certificates \
           cmake \
           curl \
           dumb-init \
           freetds-bin \
           gcc \
           g++ \
           git \
           gnupg \
           gosu \
           krb5-user \
           ldap-utils \
           libffi6 \
           libldap-2.4-2 \
           libsasl2-2 \
           libsasl2-modules \
           libssl1.1 \
           locales  \
           lsb-release \
           netcat \
           openssh-client \
           postgresql-client \
           python3-numpy \
           rsync \
           sasl2-bin \
           sqlite3 \
           sudo \
           tree \
           unixodbc \
           unzip \
           wget \
    && apt-get autoremove -yqq --purge \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ARG AIRFLOW_PIP_VERSION=21.0.1

# By default PIP has progress bar but you can disable it.
ARG PIP_PROGRESS_BAR="on"

COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir --upgrade "pip==${AIRFLOW_PIP_VERSION}" && \
    pip install --no-cache-dir -r requirements.txt && \
    rm requirements.txt

ARG AIRFLOW_HOME=/opt/airflow
ARG AIRFLOW_UID="1000"
ARG AIRFLOW_GID="1000"
ENV AIRFLOW_UID=${AIRFLOW_UID}
ENV AIRFLOW_GID=${AIRFLOW_GID}

ENV AIRFLOW__CORE__LOAD_EXAMPLES="false"

ARG AIRFLOW_USER_HOME_DIR=/home/airflow
ENV AIRFLOW_USER_HOME_DIR=${AIRFLOW_USER_HOME_DIR}

RUN addgroup --gid "${AIRFLOW_GID}" "airflow" && \
    useradd -m airflow -u "${AIRFLOW_UID}" -g "${AIRFLOW_GID}" && \
    echo 'airflow:airflow' | chpasswd airflow && \
    echo "airflow ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/airflow && \
    chmod 0440 /etc/sudoers.d/airflow

ENV AIRFLOW_HOME=${AIRFLOW_HOME}

# Make Airflow files belong to the root group and are accessible. This is to accommodate the guidelines from
# OpenShift https://docs.openshift.com/enterprise/3.0/creating_images/guidelines.html
RUN mkdir -pv "${AIRFLOW_HOME}"; \
    mkdir -pv "${AIRFLOW_HOME}/dags"; \
    mkdir -pv "${AIRFLOW_HOME}/logs"; \
    chown -R "airflow:root" "${AIRFLOW_USER_HOME_DIR}" "${AIRFLOW_HOME}"; \
    find "${AIRFLOW_HOME}" -executable -print0 | xargs --null chmod g+x && \
        find "${AIRFLOW_HOME}" -print0 | xargs --null chmod g+rw

COPY --chown=airflow:root scripts/entrypoint.sh /entrypoint
COPY --chown=airflow:root scripts/clean-logs.sh /clean-logs
RUN chmod a+x /entrypoint /clean-logs


# Make /etc/passwd root-group-writeable so that user can be dynamically added by OpenShift
# See https://github.com/apache/airflow/issues/9248
RUN chmod g=u /etc/passwd

ENV PATH="${AIRFLOW_USER_HOME_DIR}/.local/bin:${PATH}"
ENV GUNICORN_CMD_ARGS="--worker-tmp-dir /dev/shm"

WORKDIR ${AIRFLOW_HOME}

EXPOSE 8080

USER ${AIRFLOW_UID}

ARG GIT_CONFIG_USER_NAME
ARG GIT_CONFIG_EMAIL
RUN git config --global user.name ${GIT_CONFIG_USER_NAME} && \
    git config --global user.email ${GIT_CONFIG_EMAIL}

ENTRYPOINT ["/usr/bin/dumb-init", "--", "/entrypoint"]
CMD ["--help"]